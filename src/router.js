import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import ListView from 'routes/ListView';

export default function ({history }){
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={ListView} />
      </Switch>
    </Router>
  );
}
