import React from 'react';
import { connect } from 'dva';
import styles from './styles.less';
import { Carousel } from 'antd-mobile';

const NewsCarousel = ({ dispatch, newdetail, ...rest }) =>{
    return(
        <Carousel
            autoplay={false}
            infinite
            selectedIndex={0}
            swipeSpeed={35}>
            {newdetail && newdetail.length!== 0?
                newdetail.map(val =>(
                    <a 
                        key={val.Id} 
                        href={val.Link} 
                        className={styles.banner}>
                        <img 
                        style={{ 
                            width: '100%', 
                            height: '180px'
                        }}
                        src={val.Img} 
                        alt=""/>
                        <p className={styles.title}>
                            {val.Title}
                        </p>
                    </a>
                )): ""}
        </Carousel>
    )
} 

export default connect(({ products }) => ({ ...products }))(NewsCarousel);