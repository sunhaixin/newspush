import React from 'react';
import { connect } from 'dva';
import styles from './styles.less';
import { Tabs, WhiteSpace, Card, WingBlank } from 'antd-mobile';

const DTabBar = Tabs.DefaultTabBar;

const NewsCrad = ({ dispatch, newinfo, newdetail, ...rest }) =>{

    const changeKey = (key) =>{
        dispatch({
           type: 'products/NewsDetail',
           payload:{ type: key.id, start: 0, limit: 1000 }
        })
    }

    const renderContent = (tab, i) => {
        return(
            <div>
            {newdetail && newdetail.length!== 0? 
                newdetail.map((val, i) =>
                <WingBlank size="sm" key={i}>
                    <WhiteSpace/>
                    <a href={val.Link}>
                        <Card>
                            <Card.Body>
                                    <img src={val.Img} className={styles.cover} alt=""/>
                                    <div className={styles.adde}>{val.Title}</div>
                                    <p className={styles.pade}>{val.Memo}</p>
                            </Card.Body>
                        </Card>
                    </a>
                </WingBlank>
             ): ""}
            </div>
        )
    }
 
    return(
        <Tabs 
            tabs={newinfo} 
            onChange={changeKey}
            renderTabBar={props => <DTabBar { ...props } />}>
            {renderContent}
        </Tabs>
    )
} 

export default connect(({ products }) => ({ ...products }))(NewsCrad);