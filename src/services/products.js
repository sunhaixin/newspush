import request from 'requesturl';

// 新闻轮播图显示
export function NewsCarousel(start, limit ){
    const body = JSON.stringify({ start: 0, limit: 1000 });
    return request("menu/query", { 
        method: "POST", body,
        headers: {'Content-Type': 'application/json'}
    });   
}

//新闻列表
export function NewsList(start, limit, type){
    const body = JSON.stringify({ start, limit, type});
    return request("new/query", {
        method: "POST", body, 
        headers: {'Content-Type': 'application/json'}
    }); 
}