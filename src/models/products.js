import * as services from '../services/products';
export default {
	
	namespace: 'products',

	state: {
    newinfo: [],
    newdetail: []
	},

	reducers: {
		update(state, action) {
	      return { ...state, ...action.payload };
	    },
    },

	effects: {
		* NewsInfo({ payload},{ call, put }){
			const list = yield call(services.NewsCarousel);
			let menu= [], arr = list.data.data;
			arr.forEach(items => {menu.push({ title: items.Text, id: items.Id }) });
			yield put({ type: "update", payload: { newinfo: menu }});
		},

    * NewsDetail({ payload:{start, limit, type}}, { call, put }){
			const detail = yield call(services.NewsList, start, limit, type);
      yield put({ type: "update", payload: { newdetail: detail.data.data}});
    }
	},

	subscriptions: {
	    setup({ dispatch, history }) {
	        return history.listen(({ pathname, query }) => {
            if(pathname === "/"){
							 dispatch({ type: 'NewsInfo'});
               dispatch({ type: 'NewsDetail', payload:{
								start: 0, 
								limit: 1000, 
								type: "7ad2c8db-ff04-4736-81c9-1b7c6fb276b3"
							}});
            }
          })
	    },
	  },
};