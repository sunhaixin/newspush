import React from 'react';
import { NewsCarousel, NewsCrad } from 'components';

export default function ListView(){
  return (
    <div>
        <NewsCarousel/>
        <NewsCrad/>
    </div>
  );
}