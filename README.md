# NewsPush

#### 项目介绍
    推送新闻列表
    
#### 软件架构

- `dva-cli`
- `antd-mobile`

#### 安装教程
- cnpm install
- cnpm install antd-mobile --save

#### 使用说明
- `cnpm start`,`yarn start`, `npm start`
- 没有`yarn`的话可以通过 `npm install -g yarn`进行下载

#### 参与贡献
- [sunhaixin](https://gitee.com/github-29425276/NewsPush)
