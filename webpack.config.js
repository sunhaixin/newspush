const path = require('path');

module.exports = (webpackConfig, env) => {
  // 别名配置
  webpackConfig.resolve.alias = {
    'components': path.join(__dirname, 'src', 'components'),
    'routes': path.join(__dirname, 'src', 'routes'),
    'requesturl': path.join(__dirname, 'src', 'utils')
  }
  return webpackConfig;
}